<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create offer';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
	<h1><?= Html::encode($this->title) ?></h1>
	<p>Please fill out the following fields to create offer:</p>
	<div class="row">
		<div class="col-lg-5">
			<form action="addoffer" id="create offer id" data-eos-form="true">
				<div class="form-group field-signupform-username required has-error">
					<label class="control-label" for="signupform-username">Account</label>
					<input type="text" class="form-control" name="self" value="ggrocket">
					<p class="help-block help-block-error"></p>
				</div>
				<div class="form-group field-signupform-username required has-error">
					<label class="control-label" for="signupform-username">Price</label>
					<input type="text" class="form-control" name="price" value="100000">
					<p class="help-block help-block-error"></p>
				</div>
				<div class="form-group field-signupform-username required has-error">
					<label class="control-label" for="key">Key</label>
					<input type="password" class="form-control" name="key" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g">
					<p class="help-block help-block-error"></p>
				</div>
				<input type="submit" class="form-control"/>
			</form>
		</div>
	</div>
</div>
