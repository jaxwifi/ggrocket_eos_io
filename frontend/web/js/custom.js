/*(async function () {
	/*
	eos = Eos({
		httpEndpoint: 'http://ayeaye.cypherglass.com:8888',
		chainId: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f',
		debug: true
	});
	*/
// // Optional configuration..
// var config = {
// 	chainId: null, // 32 byte (64 char) hex string
// 	// keyProvider: ['PrivateKeys...'], // WIF string or array of keys..
// 	httpEndpoint: 'http://127.0.0.1:8888',
// 	// mockTransactions: () => 'pass', // or 'fail'
// 	transactionHeaders: (expireInSeconds, callback) => {
// 		callback(null/*error*/, headers)
// 	},
// 	expireInSeconds: 60,
// 	broadcast: true,
// 	debug: true, // API and transactions
// 	sign: true
// };
//
// eos = Eos(config);


// Eos = require('eosjs');

// returns Promise
/*
eos = Eos({keyProvider: '5J349PotHwQcbnHGthCTNfrvxTpCHcwti3B7pZFHiaqQhsgAZ3j'});

try
{
	await eos.transaction({
		actions: [
			{
				account      : 'addressbook',
				name         : 'create',
				authorization: [{
					actor     : 'jax',
					permission: 'active'
				}],
				data         : {
					username: 'jax',
					ssn     : 123, // primary key, social security number
					fullname: 'jax test',
					age     : 13
				}
			}
		]
	})
} catch (e)
{
	console.log(JSON.parse(e));
}


})();
*/

var account = 'ggrocket';

function pushAction(key, data, action)
{
	console.log(data);
	eos = Eos({keyProvider: key});
	eos.transaction({
		actions: [
			{
				account      : account,
				name         : action,
				authorization: [{
					actor     : data["self"],
					permission: 'active'
				}],
				data         : data,
			}
		]
	}).then(function () {
		document.location.href = "/";
	});

}

function getTableRowsSeller(table_name)
{
	var eos = Eos();
	var row_options = {
		"json"     : true,
		"code"     : account,
		"scope"    : account,
		"table"    : table_name,
		"table_key": "ssn",
		// "lower_bound": 119,
		// "upper_bound": -1,
		"limit"    : 1000,
	};
	eos.getTableRows(row_options).then(function (result) {
		$.each(result.rows, function (k, v) {
			var respond_form = '';
			if (v.seller == 'none')
				respond_form = '<form action="respondoffer" data-eos-form="true">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" value="gguser2" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" class="btn-sm btn-primary" value="[S] Respond"/>' +
					'</form>';
			else if (!v.sellerdone)
				respond_form = '<form action="decisoffer" style="float: left;" data-eos-form="true" class="form-inline">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" name="decision" value="1" data-int="1"/>' +
					'<input type="hidden" value="gguser2" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" value="[S] Done!" class="btn-sm btn-success"/>' +
					'</form>'
					+
					'<form action="decisoffer" data-eos-form="true" class="form-inline">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" name="decision" value="-1" data-int="1"/>' +
					'<input type="hidden" value="gguser2" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" value="[S] Decline" class="btn-sm btn-danger"/>' +
					'</form>';
			else
			{
				if (v.buyerdone != v.sellerdone)
					respond_form = "Decline from buyer. Waiting for arbitors";
				else
					respond_form = "Finished";

			}

			$('#offers-list').append("<tr>" +
				"<td>" + v.id + "</td>" +
				"<td>" + v.price + "</td>" +
				"<td>" + v.buyer + "</td>" +
				"<td>" + v.seller + "</td>" +
				"<td scope=\"col\">" + respond_form + "</td>" +
				"</tr>");
		});
		initRespondForm();
	});
}


function getTableRowsArbitors(table_name)
{
	var eos = Eos();
	var row_options = {
		"json"     : true,
		"code"     : account,
		"scope"    : account,
		"table"    : table_name,
		"table_key": "ssn",
		// "lower_bound": 119,
		// "upper_bound": -1,
		"limit"    : 1000,
	};
	eos.getTableRows(row_options).then(function (result) {
		$.each(result.rows, function (k, v) {
			var respond_form = '';
			if (Math.abs(v.sellerdone - v.buyerdone) > 1)
			{
				respond_form = '<form action="decisoffer" style="float: left;" data-eos-form="true" class="form-inline">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" name="decision" value="1" data-int="1"/>' +
					'<input type="hidden" value="gguser4" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" value="[A] For Seller!" class="btn-sm btn-default"/>' +
					'</form>'
					+
					'<form action="decisoffer" data-eos-form="true" class="form-inline">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" name="decision" value="-1" data-int="1"/>' +
					'<input type="hidden" value="gguser4" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" value="[A] For Buyer!" class="btn-sm btn-default"/>' +
					'</form>';

				$('#offers-list').append("<tr>" +
					"<td>" + v.id + "</td>" +
					"<td>" + v.price + "</td>" +
					"<td>" + v.buyer + "</td>" +
					"<td>" + v.seller + "</td>" +
					"<td scope=\"col\">" + respond_form + "</td>" +
					"</tr>");
			}
		});
		initRespondForm();
	});
}

function getTableRowsBuyer(table_name)
{
	var eos = Eos();
	var row_options = {
		"json"     : true,
		"code"     : account,
		"scope"    : account,
		"table"    : table_name,
		"table_key": "ssn",
		// "lower_bound": 119,
		// "upper_bound": -1,
		"limit"    : 1000,
	};
	eos.getTableRows(row_options).then(function (result) {
		$.each(result.rows, function (k, v) {
			var respond_form = '';
			if (v.seller == 'none')
				respond_form = 'Waiting for seller';
			else if (v.sellerdone != 0 && v.buyerdone == 0)
				respond_form = '<form action="decisoffer" style="float: left;" data-eos-form="true" class="form-inline">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" name="decision" value="1" data-int="1"/>' +
					'<input type="hidden" value="ggrocket" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" value="[B] Done!" class="btn-sm btn-success"/>' +
					'</form>'
					+
					'<form action="decisoffer" data-eos-form="true" class="form-inline">' +
					'<input type="hidden" name="id" value="' + v.id + '"/>' +
					'<input type="hidden" name="decision" value="-1" data-int="1"/>' +
					'<input type="hidden" value="ggrocket" name="self"/>' +
					'<input type="hidden" value="5KSwXFxuvpig1EcrVsonhw2iw2ZFfbZ4hiBhjwVY1kEoGN5yn6g" name="key"/>' +
					'<input type="submit" value="[B] Decline" class="btn-sm btn-danger"/>' +
					'</form>';
			else
			{
				if (v.buyerdone != v.sellerdone)
						respond_form = "No consensus. Waiting for arbitors";
				else
						respond_form = "Finished";
			}

			$('#offers-list').append("<tr>" +
				"<td>" + v.id + "</td>" +
				"<td>" + v.price + "</td>" +
				"<td>" + v.buyer + "</td>" +
				"<td>" + v.seller + "</td>" +
				"<td scope=\"col\">" + respond_form + "</td>" +
				"</tr>");
		});
		initRespondForm();
	});
}

function getTableAccounts(table_name)
{
	var eos = Eos();
	var row_options = {
		"json"     : true,
		"code"     : "ggrocket",
		"scope"    : "ggrocket",
		"table"    : "accounts",
		"table_key": "ssn",
		// "lower_bound": 119,
		// "upper_bound": -1,
		"limit"    : 1000,
	};
	eos.getTableRows(row_options).then(function (result) {
		$.each(result.rows, function (k, v) {
			var respond_form = '';
			$('#offers-list').append("<tr>" +
				"<td>" + v.owner + "</td>" +
				"<td>" + v.balance + "</td>" +
				"<td>" + v.frozen_balance + "</td>" +
				"<td>" + v.rating + "</td>" +
				"</tr>");
		});
		initRespondForm();
	});
}


//addoffer: name price key
$(function () {
	initRespondForm();
});


function initRespondForm()
{
	$('[data-eos-form]:not(.init-d)').submit(function (event) {
		event.preventDefault();
		event.stopPropagation();

		var action = $(this).attr('action');
		var tmp = $(this).serializeArray();
		var data = {};

		var key = '';
		var account = '';

		console.log(tmp);
		$(tmp).each(function (k, v) {
			if ($('[name="' + v.name + '"][data-int]').length)
			{
				console.log("v");
				console.log(v);
				v.value = parseInt(v.value);
			}

			switch (v.name)
			{
				case "key":
					key = v.value;
					break;
				default:
					data[v.name] = v.value;
					break;
			}
		});

		pushAction(key, data, action);
	}).addClass("init-d");
}
